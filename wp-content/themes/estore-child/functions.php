<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
        wp_enqueue_style( 'chld_thm_cfg_parent', trailingslashit( get_template_directory_uri() ) . 'style.css', array(  ) );
    }
endif;
add_action( 'wp_enqueue_scripts', 'chld_thm_cfg_parent_css', 10 );

// END ENQUEUE PARENT ACTION


//user register api 

add_action( 'rest_api_init', function () {
	register_rest_route( 'wp/v2', 'users/register', array(
		'methods' => 'POST',
		'callback' => 'wp_rest_user_endpoints',
	) );
} );

add_action('rest_api_init', 'wp_rest_user_endpoints');
/**
 * Register a new user
 *
 * @param  WP_REST_Request $request Full details about the request.
 * @return array $args.
 **/
function wp_rest_user_endpoints($request) {
  /**
   * Handle Register User request.
   */
  register_rest_route('wp/v2', 'users/register', array(
    'methods' => 'POST',
    'callback' => 'wc_rest_user_endpoint_handler',
  ));
}
function wc_rest_user_endpoint_handler($request = null) {
  $response = array();
  $parameters = $request->get_json_params();
  $username = sanitize_text_field($parameters['username']);
  $email = sanitize_text_field($parameters['email']);
  $password = sanitize_text_field($parameters['password']);
  // $role = sanitize_text_field($parameters['role']);
  $error = new WP_Error();
  if (empty($username)) {
    $error->add(400, __("Username field 'username' is required.", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  if (empty($email)) {
    $error->add(401, __("Email field 'email' is required.", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  if (empty($password)) {
    $error->add(404, __("Password field 'password' is required.", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  // if (empty($role)) {
  //  $role = 'subscriber';
  // } else {
  //     if ($GLOBALS['wp_roles']->is_role($role)) {
  //      // Silence is gold
  //     } else {
  //    $error->add(405, __("Role field 'role' is not a valid. Check your User Roles from Dashboard.", 'wp_rest_user'), array('status' => 400));
  //    return $error;
  //     }
  // }
  $user_id = username_exists($username);
  if (!$user_id && email_exists($email) == false) {
    $user_id = wp_create_user($username, $password, $email);
    if (!is_wp_error($user_id)) {
      // Ger User Meta Data (Sensitive, Password included. DO NOT pass to front end.)
      $user = get_user_by('id', $user_id);
      // $user->set_role($role);
      $user->set_role('subscriber');
      // WooCommerce specific code
      if (class_exists('WooCommerce')) {
        $user->set_role('customer');
      }
      // Ger User Data (Non-Sensitive, Pass to front end.)
      $response['code'] = 200;
      $response['message'] = __("User '" . $username . "' Registration was Successful", "wp-rest-user");
    } else {
      return $user_id;
    }
  } else {
    $error->add(406, __("Email already exists, please try 'Reset Password'", 'wp-rest-user'), array('status' => 400));
    return $error;
  }
  return new WP_REST_Response($response, 123);
}

add_filter( 'woocommerce_rest_check_permissions', 'my_woocommerce_rest_check_permissions', 90, 4 );

function my_woocommerce_rest_check_permissions( $permission, $context, $object_id, $post_type){
	if($_SERVER['REQUEST_METHOD']=='GET'){
		 return true;
	}
	else{
		 return $permission;
	}
	 
}

function bbloomer_redirect_checkout_add_cart( $url ) {
	foreach ( WC()->cart->get_cart() as $cart_item ) {
		
		$_product = wc_get_product($cart_item['product_id']);
		
		if( $_product->is_type( 'phive_booking' ) ) {
		   $url = get_permalink( get_option( 'woocommerce_checkout_page_id' ) );
		} 
	}
     
    return $url;
}
 
add_filter( 'woocommerce_add_to_cart_redirect', 'bbloomer_redirect_checkout_add_cart' );

//custome end point for user login 
add_action( 'rest_api_init', 'my_register_route' );
function my_register_route() {
    register_rest_route( 'wc/v2', 'user_signup', array(
                    'methods' => 'POST',
                    'callback' => 'user_signup',
                )
            );
}
function user_signup($request) {
	$parameters = $request->get_params();
	$username = preg_replace('/([^@]*).*/', '$1', $parameters['email']);
	$meta_data=[
		'billing_first_name'    => $parameters['first_name'],
		'billing_last_name'     => $parameters['last_name'],
		'billing_address_1'    => $parameters['address'],
		'billing_city'          => 'Noida',
		'billing_state'          => 'Uttar Pardesh' ,
		'billing_postcode'      => $parameters['pincode'],
		'billing_country'       => 'IN',
		'billing_email'         => $parameters['email'],
		'billing_phone'         => $parameters['Phone'],
			
	];
	
	//verify phone number 
    $user_data =user_verify($parameters['Phone']);
	$id =$user_data[0]->data->ID ;
	if($id > 0){
		return "Phone Number Already Register ";
	}
	else{
		  $user_id = wp_create_user( $username,$parameters['password'],$parameters['email']); 
    
	     
    if ( is_wp_error( $user_id ) )
      return $user_id->get_error_message();
	if($user_id){
		
			//update user  first name and last name 
			wp_update_user([
					  'ID' => $user_id, // this is the ID of the user you want to update.
					  'first_name' =>$parameters['first_name'],
					  'last_name' => $parameters['last_name'],
			  ]);

			//update user meta
			foreach ($meta_data as $meta_key => $meta_value ) {
				update_user_meta( $user_id, $meta_key, $meta_value );
		   }

		//genrate a referal code for user 
			$randomString="ref";
				$characters = "0123456789";
			for ($i = 0; $i < 7; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
			}

			//store referal code in user meta for future refernece
			if($randomString){
				update_user_meta( $user_id, 'referral_id', $randomString );
			}
		
		
			//give reward point to user if refer code 
			if(isset($parameters['refer_id']) && !empty($parameters['refer_id'])){
				
				//fetch user id with referal_id
				   $refer_code_owner = get_users(
										array(
											 'meta_query' => array(
												array(
													'key' => 'referral_id',
													'value' => true,
													'compare' => '=='
												)
											)
										)
								   );
                  $refer_code_owner = get_users(
										array(
											 'meta_query' => array(
												array(
													'key' => 'referral_id',
													'value' => $parameters['refer_id'],
													'compare' => '=='
												)
											)
										)
								   );
                  $owner_id=$refer_code_owner[0]->data->ID;
				
				 //update referal ower reward point
			      mycred_add( 'reference', $owner_id, 20, 'Points for Reference' );
				
				//update user reward point 
				mycred_add( 'reference', $user_id, 20, 'Points for Reference' );
			}
			return $user_id;
         }
		
	}
	
	

   
}


//custome end point for phone number verification 

function user_verify($phone) {
	
	$phone_number =$phone;
	
	//$user = get_users(array('meta_key' =>'billing_phone', 'meta_value' =>$phone));
	
	$user_query = new WP_User_Query(
		array(
			'meta_key'	  =>'billing_phone',
			'meta_value'	=>$phone_number
		)
	);

	
	$users = $user_query->get_results();
    //$user_id=$user_data[0]->data->ID;
	return $users;
	
}
add_filter('flush_rewrite_rules_hard','__return_false');


//custome end point for customer registation 


/*function custome_register( WP_REST_Request $request ) {
    
    
    // You can access parameters via direct array access on the object:
    $email = $request['email'];
    $username = $request['username'];
    $password = $request['password'];
    
    $user_id = wp_create_user( $username, $password, $email );
    
    if ( is_wp_error( $user_id ) )
   return $user_id->get_error_message();
    
    if($user_id>0){
        return "user Create Successfully ";
    }
}


add_action( 'rest_api_init', function () {
  register_rest_route( 'wc/v2/','register/', array(
    'methods' => 'POST',
    'callback' => 'custome_register',
  ) );
} );
*/

//return user id with tokan 
function jwt_auth_custome_function($data, $user) {
    $data['user_id'] = $user->ID;
   
    return $data;
}
add_filter( 'jwt_auth_token_before_dispatch', 'jwt_auth_custome_function', 10, 2 );


//custome end point for user reword 
add_action( 'rest_api_init', function () {
  register_rest_route( 'wc/v2/','user_reward', array(
    'methods' => 'POST',
    'callback' => 'user_reward_point',
  ) );
} );
function user_reward_point($request) {
	$parameters = $request->get_params();
	
	$user_id=$parameters['id'];
	$total_amount=$parameters['total_amount'];
     
    //get user reward point 
    $reward = mycred_get_users_cred($user_id);
    
    
    //get User wallet balance
    $request = new WP_REST_Request( 'GET', '/wp/v2/current_balance/'.$user_id );
    $response = rest_do_request( $request );
    $server = rest_get_server();
    $wallet_balance = $server->response_to_data( $response, false );
    
    
    //current we are apply 10% deduction to total amount
    
    $percentage_apply=10;
   
    $deduct_by_point= ($total_amount*$percentage_apply)/100;
   
    if($deduct_by_point <= $reward){
        $point_use =true;
        
    }
    else{
        $point_use =false;
        $deduct_by_point=0;
    }
        
    
    $tatal_payable_amount=$total_amount-$deduct_by_point;
    
    $result=array(
                  'current_wallet_balance'=>$wallet_balance,
                  'current_total_reward_point'=>number_format((double)$reward, 2, '.', ''),
                  'Point_appleid'=>$point_use,
                  'total_deduction'=>sprintf("%.2f", $deduct_by_point),
                  'total_payable_amount'=>sprintf("%.2f", $tatal_payable_amount)    
                );
    
    return  $result;
}


//custome end point for user reword  only
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','user_point', array(
	  'methods' => 'POST',
    'callback' => 'user_point',
	
  ) );
} );
function user_point($request) {
	$parameters = $request->get_params();
	
	$user_id=$parameters['id'];
	 
    //get user reward point 
    $reward = mycred_get_users_cred($user_id);
    return  $reward;
}


//custome end point for user point deduction
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','user_point_detect', array(
    'methods' => 'POST',
    'callback' => 'user_point_detect',
  ) );
} );
function user_point_detect($request) {
	$parameters = $request->get_params();
	$user_id=$parameters['id'];
	$total_amount=$parameters['total_amount'];
     $percentage_apply=10;
   
    $deduct_by_point= ($total_amount*$percentage_apply)/100;
   
    if($deduct_by_point <= $reward){
        
        mycred_subtract( 'reference', $user_id, -$deduct_by_point, 'Product' );
		return "success";
    }
    else{
        return "Failure" ;
    }
}


//custome end point for order create 
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','after_order', array(
    'methods' => 'POST',
    'callback' => 'after_order_update',
  ) );
} );
function after_order_update($request) {
	$parameters = $request->get_params();
	$point_applied=$parameters['point_applied'];
	$user_id=$parameters['user_id'];
	$order_id=$parameters['order_id'];
	$payment_type=$parameters['payment_type'];
	$result=array();
	
	$result['point_apply']="point is not applicable";
	if($point_applied){
		   $total_deduction=$parameters['total_point'];
		   if($total_deduction > 0){
			    if(mycred_subtract( 'reference', $user_id, $total_deduction, 'Product' )){
					 $result['point_apply']="true";
				}
                else{
					  $result['point_apply']="false";
				}			    
		   }
     }
	 
	 if($payment_type=='wallet'){
		  $type='debit';
		  $wallet_amount=$parameters['wallet_amount'];
		  $detail="For Payment of order ".$order_id;
		 
		  // new wallet transaction 
		  
		   $transaction_id=wallet_transaction($user_id,$wallet_amount,$detail);
		
		 
		  if($transaction_id > 0){
			  //update order with transaction id
			    $result=update_order_processing($order_id,$transaction_id);
			  
		  }
	 }
	 elseif($payment_type=='paytm'){
		 $transaction_id=$parameters['transaction_id'];
		 
		  if($transaction_id > 0){
			  //update order with transaction id
			  $result=update_order_processing($order_id,$transaction_id);
			  
		  }
		  else{
			  if($order_id){
				  $result['order_id']= $order_id;
				  $result['order_update'] ="success";
			  }
		  }
		 
	 }
}


//custome end point for jwt login using phone number and password 
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','user_login', array(
    'methods' => 'POST',
    'callback' => 'user_login',
  ) );
} );
function user_login($request) {
	$parameter = $request->get_params();
	$phone_number = $parameter['phone'];
	$password = $parameter['password'];
	//$user = get_users(array('meta_key' =>'billing_phone', 'meta_value' =>$phone));
	
	$user_query = new WP_User_Query(
		array(
			'meta_key'	  =>'billing_phone',
			'meta_value'	=>$phone_number
		)
	);

	
	$users = $user_query->get_results();
	foreach($users as $user ){
		$username=$user->data->user_login;
	}
     
	
	 //user login using jwt 
    $request = new WP_REST_Request( 'POST', '/jwt-auth/v1/token');
	 $request->set_query_params( [ 'username' => $username,
									  'password' =>$password
									   ] );
    $response = rest_do_request( $request );
    
    return $response;	
}

//Coupon Code validate by sunny
/*add_action( 'woocommerce_applied_coupon', array( WC_Cart, 'calculate_totals' ), 10, 0 );
    register_rest_route( '/wc/v2/', 'user_coupon', array(
                    'methods' => 'GET',
                    'callback' => 'user_valid_coupon',
                )
            );
function user_valid_coupon($request) {         
	$parameters = $request->get_params();
	$coupon_code = $parameters['cart_total'];
	
	//custome end point for new wallet transaction 
		  $request = new WP_REST_Request( 'POST', '/wp/v2/wallet/'.$user_id );
		  $request->set_query_params( [ 'type' => 'debit',
									  'amount' =>"$wallet_amount",
									   'detial' =>"$detail"
									  ] );
          $response = rest_do_request( $request );
          $server = rest_get_server();
          $wallet_transaction = $server->response_to_data( $response, false );
		  
	
	$result=array();
	
	$coupon = new \WC_Coupon( $coupon_code );   
    $discounts = new \WC_Discounts( WC()->cart );
    $valid_response = $discounts->is_coupon_valid( $coupon );
    if ( is_wp_error( $valid_response ) ) {
        //return new WP_Error( 'invalid_coupon', $valid_response->get_error_message(), array( 'status' => 401 ) );
		//return "Failure";
		$result['status'] = "Failure";
		$result['code'] = 401;
		$result['message'] = $valid_response->get_error_message();
    } else {
        //return new WP_Error( 'success', $valid_response->get_error_message(), array( 'status' => 0 ) );
		//return "Success";
		$result['status'] = "Success";
		$result['code'] = 0;
		$result['message'] = '';
    }
	return $result;
}*/

/////////////////////// PM's Changes Start /////////////////////////

/*
add_action('rest_api_init', function() {
	register_rest_route('/wc/v2/', 'coupon_validate', array(
                    'methods' => 'GET',
                    'callback' => 'is_coupon_valid',
					'args' => array(
					'id' => array(
						'validate_callback' => function($param, $request, $key) {
						  return is_numeric( $param );
						}
					  ),
					),
                ));
});
*/


/*add_action('rest_api_init', function() {
	register_rest_route( '/wc/v2/', 'coupon_validate', array(
			'methods' => 'POST',
			'callback' => 'is_coupon_valid',
			)
		); 
});*/

/*
function is_coupon_valid($request) {
	
	$parameters = $request->get_params();
	$coupon_code = $parameters['coupon_code'];
	
	$coupon = new \WC_Coupon( $coupon_code );   
    $discounts = new \WC_Discounts( WC()->cart );
    $valid_response = $discounts->is_coupon_valid( $coupon );
    if ( is_wp_error( $valid_response ) ) {
        //return new WP_Error( 'invalid_coupon', 'invalid coupon', array( 'status' => 401 ) );
		return "Failure";
    } else {
        //return new WP_Error( 'success', 'coupon success', array( 'status' => 0 ) );
		return "Success";
    }
}
*/

////////////////////// PM's Changes End ///////////////////////////
//
//shikha changes
add_action( 'rest_api_init', 'user_coupan_route' );
 function user_coupan_route() {
    register_rest_route( 'wc/v2', 'user_coupon', array(
                    'methods' => 'POST',
                    'callback' => 'user_valid_coupon',
                )
            );
 }
function user_valid_coupon() {      

   //get All coupan list using internal rest route 
     $request = new WP_REST_Request( 'GET', '/wc/v3/coupons');
     $response = rest_do_request( $request );
     $server = rest_get_server();
     $coupan_list = $server->response_to_data( $response, false );	
	 $result=array();	
	
	//validate each coupan
	foreach ($coupan_list as $coupan){
			
	   $discounts = new \WC_Discounts(WC()->cart);	
	   $coupon_code = new \WC_Coupon($coupan['code']); 
		
         $valid_response = $discounts->is_coupon_valid($coupon_code);
		if ( is_wp_error( $valid_response ) ) {
	
		} else {
	
			array_push($result,$coupan);
			
		}
		 
	}
	
	return $result;
   
}



//custome end point for ticket creation
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','add_ticket', array(
    'methods' => 'POST',
    'callback' => 'add_ticket_bu_user',
  ) );
} );
function add_ticket_bu_user($request) {
	$parameters = $request->get_params();
	$title=$parameters['title'];
	$content=$parameters['content'];
	$author=$parameters['user_id'];
	$order_id=$parameters['order_id'];
	  
			  $request = new WP_REST_Request( 'POST', '/wpas-api/v1/tickets');
		      $request->set_query_params( [ 'title' => $title,
									        'content' =>$content,
										     'author'=>$author
									   ] );
              $response = rest_do_request( $request );
              $server = rest_get_server();
              $ticket = $server->response_to_data( $response, false );
		      $ticket_id=$ticket['id'];
			  if($ticket_id){
				  update_post_meta($ticket_id,"_order_id",$order_id);
				  return $ticket;
			  }
	
}




//custome end point for testing
		add_action( 'rest_api_init', function () {
		  register_rest_route( '/wc/v2/','testing', array(
			'methods' => 'GET',
			'callback' => 'testing_endpoint',
		  ) );
		} );
		function testing_endpoint($request) {
			//update some order as schedule order
// 			update_post_meta( 836, '_schedule_order', 'true');
// 			update_post_meta( 809, '_schedule_order', 'true');
// 			update_post_meta( 804, '_schedule_order', 'true');
// 			
           //  update_post_meta(931, '_delivery_date', '2019-04-09');
			 //update_post_meta( 809, '_schedule_order', 'true'); 	
			//$user_data= get_userdata(1);
	
        
		/*	$filter=array(
				'post_type'        => 'shop_order',
				
			);
			  
		    $query_args = array(
                  'post_type'      => 'shop_order',
                  'post_status'    => 'wc-pending',
                  'posts_per_page' => 999999999999,
				  'meta_query' => array(
					  
						array(
							 'key' => '_delivery_date',
							 'value' => "2019-02-14",

						)
			      )
               );
         
    $all_orders      = get_posts( $query_args );
			$order = wc_get_order( $all_orders[0]->ID );
			$order_data = $order->get_data();
			//return $order_data;
			//print_r($all_post); */
			//$phone=8470931279;
			//$user_data=user_verify($phone);
			
			//return $user_data;
			//return "hello shikha ";
			//
			
			  
		      //print_r($cusotmer[meta_data]);
		}
			



//filter order base on input param 

add_action( 'pre_get_posts', 'filter_result_by_order_area');
function filter_result_by_order_area( $query ) {
	 	 
	global $wpdb;
	global $typenow;
	
   
   if (isset( $_GET['type'] ) && ! empty( $_GET['type']) ) {
	  
	   if($_REQUEST['type']=="child"){
			 // Add your criteria
			$parent_id = $_REQUEST['parent_id'];
		     
		    if(isset($_REQUEST['delivery_date'] ) && ! empty($_REQUEST['delivery_date'])){
				  $meta_query = array(
					  
						array(
							 'key' => '_delivery_date',
							 'value' => $_REQUEST['delivery_date'],
						),
					  array(
						 'key' => '_customer_user',
						 'value' => $_REQUEST['customer_id'],
						 'compare'=>'=',				
					    )
			     );  
				$query->set('meta_query',$meta_query);
				
			}
		 
			
			// Set the meta query to the complete, altered query
			$query->set('post_parent',$parent_id);
			
		}
	    if($_REQUEST['type']=="normal"){
			$meta_query = array(
				   
					array(
						 'key' => '_schedule_order',
						 'compare'=>'NOT EXISTS',
						),
				      array(
						 'key' => '_customer_user',
						 'value' => $_REQUEST['customer_id'],
						 'compare'=>'=',				
					)
			);    

			// Set the meta query to the complete, altered query
			
			$query->set('meta_query',$meta_query);	
			$query->set('post_parent',0);
		}
	   //if order is schedule type then a _schedule_order meta key is store in parent type order
	    if($_REQUEST['type']=="schedule"){
			$meta_query = array(
				   
					array(
						 'key' => '_customer_user',
						 'value' => $_REQUEST['customer_id'],
						 'compare'=>'=',				
					),
				array(
						 'key' => '_schedule_order',
						 'value' => 'true'
										
					)
			);    
            
			// Set the meta query to the complete, altered query
			$query->set('meta_query',$meta_query);	
			
		}
     
   } 
	
 }

//add custome meta field in ticket
add_action( 'rest_api_init', 'create_api_posts_meta_field' );
 
function create_api_posts_meta_field() {
 
		 // register_rest_field ( 'name-of-post-type', 'name-of-field-to-return', array-of-callbacks-and-schema() )
		 register_rest_field( 'ticket', 'meta', array(
		 'get_callback' => 'get_post_meta_for_api',
		 'schema' => null,
		 )
		 );
     }
	function get_post_meta_for_api( $object ) {
		 //get the id of the post object array
		 $post_id = $object['id'];

		 //return the post meta
		 return get_post_meta( $post_id );
}


// function to remove mycred_default_history from user detail 
add_action( 'rest_api_init', 'register_posts_meta_field' ); 
function register_posts_meta_field() {  
	$args = array(
		
		'single'=>true,
		'object_subtype'=>'user',
       'show_in_rest' =>false,
      );
 register_meta( 'user', '_current_woo_wallet_balance', $args );
	//register_meta( 'user', 'mycred_default_history', '__return_false' );
}


//custome end point for schedule order  creation
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','create_order', array(
    'methods' => 'POST',
    'callback' => 'create_schedule_order',
  ) );
} );
function create_schedule_order($request){
	$parameters = $request->get_params();
	
	
	//prepare order object 
	$data = array(
		    "customer_id"=>$parameters['customer_id'],
            "billing"=>array(
			                 "first_name"=>$parameters['billing']['first_name'],
                             "last_name" =>$parameters['billing']['last_name'],
                             "address_1" =>$parameters['billing']['address_1'],
                             "address_2"=>$parameters['billing']['address_2'],
							 "city" => $parameters['billing']['city'],
							 "state" =>$parameters['billing']['state'],
							 "postcode" =>$parameters['billing']['postcode'],
							 "country" =>$parameters['billing']['country'],
							 "email" =>$parameters['billing']['email'],
							 "phone" =>$parameters['billing']['phone']							 
			            ),
			"shipping"=>array(
			                 "first_name"=>$parameters['shipping']['first_name'],
                             "last_name" =>$parameters['shipping']['last_name'],
                             "address_1" =>$parameters['shipping']['address_1'],
                             "address_2"=>$parameters['shipping']['address_2'],
							 "city" => $parameters['shipping']['city'],
							 "state" =>$parameters['shipping']['state'],
							 "postcode" =>$parameters['shipping']['postcode'],
							 "country" =>$parameters['shipping']['country']
			            ),					
        );

	$line_items=$parameters['line_items'];
	
	$data['line_items']=array();
	foreach($line_items as $item){
		
		 array_push($data['line_items'],$item) ;
	}
    
	//create a parent order 
	
	          $request = new WP_REST_Request( 'POST', '/wc/v3/orders');
		      $request->set_query_params($data);
              $response = rest_do_request( $request );
              $server = rest_get_server();
              $parent_order = $server->response_to_data( $response, false );
		      $parent_id= $parent_order['id'];
	          
			  if($parent_id){
				  
				  //if order is schedule type then a _schedule_order meta key is store in parent type order
				  update_post_meta($parent_id,"_schedule_order",'true');
				  
				  //add start date and end date in parent order 
				  $start_date =$parameters['start_date'];
				  $end_date =$parameters['end_date'];
				  
				  
				  
				  //now create child order for this parent order for each date 
				  
				  $data += ['parent_id'=> $parent_id];
				  
				  //if end date is not define the we just create order for 30 Days 
				  if($end_date==''){
					    $today = date('y-m-d');
                        $end_date = date('Y-m-d', strtotime($today . ' +30 day'));
				    }
				 
				  //update parent order with start date and end date
				  update_post_meta($parent_id,"start_date",$start_date);
				  update_post_meta($parent_id,"end_date",$end_date);
				  
				  $order_type=$parameters['type'];
				  
				  //if type is daily...then create order for all 30 days 
				  
				  if($order_type=='daily'){
					  
					    //update parent order with order type 
					    update_post_meta($parent_id,"order_type",$order_type);  
					  
					     while(strtotime($start_date) <= strtotime($end_date)){
							
                           $request = new WP_REST_Request( 'POST', '/wc/v3/orders');
		                     $request->set_query_params($data);
                             $response = rest_do_request( $request );
                             $server = rest_get_server();
                             $child_order = $server->response_to_data( $response, false ); 
					         $child_id=$child_order['id'];
					         
					         //add a custom meta field into order 
					      update_post_meta($child_id,"_delivery_date",$start_date); 
					  
					       $start_date = date('Y-m-d', strtotime($start_date . ' +1 day'));
                        }
				  }
				  
				  //if type is  'Daily excluding weekends' ..then create order from manday to friday 
				  if($order_type=='daily_exclude_weekends'){
					  
					    //update parent order with order type 
					    update_post_meta($parent_id,"order_type",$order_type); 
					  
					  //define days to exclude 
					    $weekends=array("Sat","Sun");
						 
					     while(strtotime($start_date) <= strtotime($end_date)){
							 
							 //find date for a date to exclude satuarday and sunday
							 
							 $timestamp = strtotime($start_date);
                             $day = date('D', $timestamp);
							 
							 //array_search return integer if day find in array either return boolean if fail 
							 $index=array_search($day,$weekends);
                             
                             if(is_bool($index) === true){
								 
								 $request = new WP_REST_Request( 'POST', '/wc/v3/orders');
								 $request->set_query_params($data);
								 $response = rest_do_request( $request );
								 $server = rest_get_server();
								 $child_order = $server->response_to_data( $response, false ); 
								 $child_id=$child_order['id'];
								 
								 //add a custom meta field into order 
								 update_post_meta($child_id,"_delivery_date",$start_date);
							}
       
					        $start_date = date('Y-m-d', strtotime($start_date . ' +1 day'));
                        }
				  }
				  
				  //deliver order on a particular day in a week 
				  if($order_type=='weekly'){
					  
					  //get day to deliver data
					    $delivery_day=$parameters['day'];
					
					    //update parent order with order type 
					    update_post_meta($parent_id,"order_type",$order_type);
					    update_post_meta($parent_id,"order_delivery_day",$delivery_day);
					  
					     while(strtotime($start_date) <= strtotime($end_date)){
							 
							 //find date for a date to exclude satuarday and sunday
							 
							 $timestamp = strtotime($start_date);
                             $day = date('D', $timestamp);
							 
							
                             
                             if($delivery_day===$day){
								 
								 $request = new WP_REST_Request( 'POST', '/wc/v3/orders');
								 $request->set_query_params($data);
								 $response = rest_do_request( $request );
								 $server = rest_get_server();
								 $child_order = $server->response_to_data( $response, false ); 
								 $child_id=$child_order['id'];
								 
								 //add a custom meta field into order 
								 update_post_meta($child_id,"_delivery_date",$start_date);
							}
       
					        $start_date = date('Y-m-d', strtotime($start_date . ' +1 day'));
                        }
				  }
				  
				  //deliver order on a particular day in two week 
				  if($order_type=='bi-weekly'){
					  
					  //define days to exclude 
					    $delivery_day=$parameters['day'];
					
					    //update parent order with order type 
					    update_post_meta($parent_id,"order_type",$order_type);
					    update_post_meta($parent_id,"order_delivery_day",$delivery_day);
					  
					     while(strtotime($start_date) <= strtotime($end_date)){
							 						
							 
							 $timestamp = strtotime($start_date);
                             $day = date('D', $timestamp);
							 							                             
                             if($delivery_day===$day){
								 
								 $request = new WP_REST_Request( 'POST', '/wc/v3/orders');
								 $request->set_query_params($data);
								 $response = rest_do_request( $request );
								 $server = rest_get_server();
								 $child_order = $server->response_to_data( $response, false ); 
								 $child_id=$child_order['id'];
								 
								 //add a custom meta field into order 
								 update_post_meta($child_id,"_delivery_date",$start_date);
							}
       
					        $start_date = date('Y-m-d', strtotime($start_date . ' +14 day'));
                        }
				  }  
				        
			  }
	   return $parent_order;
	
  }

//custome end point to update order on daily basis
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','update_order_job', array(
    'methods' => 'GET',
    'callback' => 'update_child_order',
  ) );
} );
function update_child_order(){
	wp_set_current_user(1);
	//get all order with delivery date is today's date 
	 $query_args = array(
                  'post_type'      => 'shop_order',
                  'post_status'    => 'wc-pending',
                  'posts_per_page' => 999999999999,
				  'meta_query' => array(
					  
						array(
							 'key' => '_delivery_date',
							 'value' =>date("Y-m-d"),

						)
			      )
               );
         
            $all_orders  = get_posts( $query_args );
	        
	        foreach($all_orders as $order){
				$order = wc_get_order($order->ID );
			    $order_data = $order->get_data();
				$order_customer_id = $order_data['customer_id'];
				
				$order_total=$order_data['total'];
				
				// get Customer wallet balance 
				
						$request = new WP_REST_Request( 'GET', '/wp/v2/current_balance/'.$user_id );
						$response = rest_do_request( $request );
						$server = rest_get_server();
						$wallet_balance = $server->response_to_data( $response, false );
				
				//check if customer balance is greate then total 
				if($wallet_balance >= $order_total){
					echo "user has sufficient balance";
					$detail ="Payment for order #".$order->ID;
					
					//create a wallet transaction and update order 
					 $transaction_id=wallet_transaction($order_customer_id,$order_total,$detail);
					
					//update order with transaction id 
					$method="wallet";
					$title="Wallet payment";
					$result =update_order($order->ID,$transaction_id,$method,$title);
				}
				else{
					$method="COD";
					$title="Cash on delivery";
					$transaction_id=0;
					$result =update_order($order->ID,$transaction_id,$method,$title);
				}
				
				return  $result;    
			
			}
			
	
  }

   function wallet_transaction($user_id,$amount,$detail){
	       $amount=$amount;
	       $detail = $detail;
	      
	        $param  = array(
				            'type' => 'debit',
							'amount' =>$amount,
							'detial' =>$detail
			            );
	   
	      $request = new WP_REST_Request( 'POST', '/wp/v2/wallet/'.$user_id);
		  $request->set_query_params($param);
          $response = rest_do_request( $request );
          $server = rest_get_server();
          $wallet_transaction = $server->response_to_data( $response, false );
	     
		  $transaction_id=$wallet_transaction['id'];
	      return $transaction_id;
   }

   function update_order($order_id,$transaction_id,$method,$tile){
	    
	         if($transaction_id > 0){
				 $param  = array(
				            'payment_method'=>$method,
				            'payment_method_title'=>$tile,
				            'status' => 'processing',
						    'transaction_id' =>"$transaction_id"
			            );
			 }
	         else{
				 $param  = array(
				            'payment_method'=>$method,
				            'payment_method_title'=>$tile,
				            'status' => 'processing',
						    
			            );
			 }
	          
			                              
			 $request = new WP_REST_Request( 'PUT', '/wc/v3/orders/'.$order_id);					   
		      $request->set_query_params($param);
              $response = rest_do_request( $request );
              $server = rest_get_server();
              $order_update = $server->response_to_data( $response, false );
	          print_r($order_update);
		      $updated_order_id=$order_update['id'];
			  if($updated_order_id){
				  $result['order_id']= $updated_order_id;
				  $result['order_update'] ="success";
			  }
	        return $result;
         } 

  function update_order_processing($order_id,$transaction_id){
	          $request = new WP_REST_Request( 'PUT', '/wc/v3/orders/'.$order_id);
		      $request->set_query_params( [ 'status' => 'processing',
									  'transaction_id' =>"$transaction_id"
									   ] );
              $response = rest_do_request( $request );
              $server = rest_get_server();
              $order_update = $server->response_to_data( $response, false );
		      $updated_order_id=$order_update['id'];
			  if($updated_order_id){
				  $result['order_id']= $updated_order_id;
				  $result['order_update'] ="success";
			  }
	         return $result;
  }

//custom end point to get user detail 
add_action( 'rest_api_init', function () {
		  register_rest_route( '/wc/v2/','get_user', array(
			'methods' => 'GET',
			'callback' => 'get_user_detail',
		  ) );
		} );
		function get_user_detail($request) {
			    $parameters = $request->get_params();
			    $user_id = $parameters['customer_id'];
			    
			    $request = new WP_REST_Request( 'GET', '/wc/v3/customers/'.$user_id);
				  $request->set_query_params($data);
				  $response = rest_do_request( $request );
				  $server = rest_get_server();
				  $cusotmer = $server->response_to_data( $response, false );

				   foreach($cusotmer[meta_data] as $meta){

					  if($meta->key=='mycred_default_history')
					   {

						   $index=array_search($meta,$cusotmer[meta_data]);
						   //unset($cusotmer[meta_data][2]);
						   array_splice($cusotmer[meta_data],$index,$index);
					   }

				   }
				   return $cusotmer;
			  

		}

//disable admin bar for shop manager 
add_action('show_admin_bar', 'disable_admin_bar');

function disable_admin_bar() {
    if (current_user_can('shop_manager')) {
        return false;
    }
}

//create custom for buisness logic by sunny
add_action('admin_menu', 'custom_menu');  
  function custom_menu() { 
  add_menu_page( 
      'Business Logic', 
      'Business Logic', 
      'administrator', 
      'business_logic', 
      'business_logic_page'
       );
if(isset( $_POST['Update'] )) {

// create post object with the form values
$inviteamount= $_POST['reward_owner_points'];
$acceptamount = $_POST['reward_user_points'];
$cartpointsdeduct = $_POST['cart_deduct_points'];
	//print_r($cartpointsdeduct);die;
  $query_args = array(
        'post_title'        =>  'Business Logic',     
	  'post_type'        =>  'business_logic',
            );
	
   $post = get_posts( $query_args );
    $post_id = $post['0']->ID;
	//print_r($post_id);die;
    update_post_meta($post_id, 'reward_owner_points', $inviteamount);
    update_post_meta($post_id, 'reward_user_points', $acceptamount);
	update_post_meta($post_id, 'cart_deduct_points', $cartpointsdeduct);
	
	get_post_meta($post_id, 'cart_deduct_points', false);
	
	//print_r($post_id);die;
}	  
	  function business_logic_page(){
		   echo "<h3>Add Buisness Logic For referal points & cart deduction points </h3>";
		    ?>
		    <form  name="config_actions" action="" method="post">
				                    <div class="row">
						<label for="title"><strong>Referral points : </strong></label> <br>
					  <div class="row">
<input type="text" name="reward_user_points"    placeholder="Enter a referral points" style=" width: 46%;"  >
					  </div>
					</div>
						
					 <div class="row">
						<label for="title"><strong> User points : </strong></label> <br>
					  <div class="row">
<input type="text" name="reward_owner_points"   placeholder="Enter a user points" style=" width: 46%;" >
					  </div>
					</div>	
				 <div class="row">
						<label for="title"><strong> cart percentage Deduction points : </strong></label> <br>
					  <div class="row">
<input type="text" name="cart_deduct_points"   placeholder="Enter a cart percentage deduct points" style=" width: 46%;" >
					  </div>
					</div>	
				
				<input type="hidden" id="custom_action" name="custom_action" value="update" />
				<input type="submit" id="my_main_action" name="Update" value="Update" />
				
			</form>
           <?php
	  }
  }



//custome menu for client_admin

add_filter( 'wp_nav_menu_items', 'add_loginout_link', 10, 2 );

function add_loginout_link( $items, $args ) {

        if (is_user_logged_in() ) {
	   
				$user = wp_get_current_user();
				if(in_array('client_admin', (array) $user->roles ) ){
				$items .= '<li><a href="'. get_home_url() .'/wp-admin/" target="_blank" >DashBord</a></li>';
		}
  
  }
  return $items;

}

//custom end point to create child order on daily basis for 30 days ahead from today date
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','create_child_order_job', array(
    'methods' => 'GET',
    'callback' => 'create_child_order',
  ) );
} );
function create_child_order(){
	wp_set_current_user(1);
	
	//get all parent order to create child 
	 $query_args = array(
                  'post_type'      => 'shop_order',
                  'post_status'    => 'wc-pending',
                  'posts_per_page' => 999999999999,
				  'meta_query' => array(
					  
						array(
						 'key' => '_schedule_order',
						 'value' => 'true'
										
					    ),array(
						 'key' => 'end_date',
						 'value' => ''
										
					)
			      )
               );
         
            $all_orders  = get_posts( $query_args );
	        foreach($all_orders as $order){
				$parent_id= $order->ID;
				$delivery_date = date('Y-m-d', strtotime('today +30 day'));
				
//fetch  child order for  this delivery date. if child exist then okay other wise we should create a child for this         //parent order 
			     $query_args = array(
                  'post_type'      => 'shop_order',
                  'post_status'    => 'wc-pending',
                  'posts_per_page' => 999999999999,
				  'post_parent'    =>$parent_id,
				  'meta_query' => array(
					  	array(
							 'key' => '_delivery_date',
							 'value' => $delivery_date,
						)
					)
			      
               );
            
	           $child_orders  = get_posts( $query_args );
				
				
					if(empty($child_orders)){
						
							//their is no child order for this date . we have to create a child order for this date 
							//
							//get order data to repeat same order 
							 $order = wc_get_order($parent_id);
							 $order_data = $order->get_data();
							 return  $order_data;
							 $billing= $order_data['billing'];
							 $shipping =$order_data['shipping']; 
							 $line_items=$order_data['line_items'];

							//prepare data to create order 
							$data = array(
										 "customer_id"=>$order_data['customer_id'],
								          "parent_id" =>$parent_id,
										 "billing"=> $billing,
										 "shipping"=>$shipping,
								         "line_items"=>$line_items
									   );
						    
						     //create child order
						     $request = new WP_REST_Request( 'POST', '/wc/v3/orders');
		                     $request->set_query_params($data);
                             $response = rest_do_request( $request );
                             $server = rest_get_server();
                             $child_order = $server->response_to_data( $response, false ); 
					         $child_id=$child_order['id'];
					         //add a custom meta field into order
					         if($child_id) {
								 update_post_meta($child_id,"_delivery_date",$delivery_date);
								 
								 //update parent order end date 
								 update_post_meta($parent_id,"end_date",$delivery_date);
							 }
					          
					}
				    
			}
   }

//custom end point to delete all pending payment child order for after disabling  
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','delete_child_order', array(
    'methods' => 'POST',
    'callback' => 'delete_all_child_order',
  ) );
} );
function delete_all_child_order($request){
	  $parameters = $request->get_params();
	  $parent_id=$parameters['parent_id'];
	
	  //get all parent order to create child 
	        $query_args = array(
                  'post_type'      => 'shop_order',
                  'post_status'    => 'wc-pending',
                  'posts_per_page' => 999999999999,
		          'post_parent'    =>$parent_id,
				  
               );
	      $child_orders=get_posts($query_args);
	      
	      //now cancel all order one by one
	      
	      foreach($child_orders as $order){
			   $order = new WC_Order($order->ID);
               $order->update_status('cancelled', 'scheduling is disabled for this order');
		  }
  }


//hooks to deduct user point after order create
/*add_action( 'woocommerce_new_order', 'my_status_pending',  1, 1  );
	function my_status_pending($order_id){
	           $order = wc_get_order($order_id);
		       
		       //$order_data = $order->get_data();
		       $order_data_line = $order->get_line_total();
		       
		       print_r($order_data_line);
		       
	}

*/

//custom end point to fetch categories excluding scheduling categories 
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','categories', array(
    'methods' => 'GET',
    'callback' => 'fetch_categories',
  ) );
} );
function fetch_categories(){
	    $request = new WP_REST_Request( 'GET', '/wc/v3/products/categories');
	    $request->set_query_params(array('per_page'=>50,
										  'parent'=>0
		                                ));
		$response = rest_do_request( $request );
        $server = rest_get_server();
        $categories = $server->response_to_data( $response, false ); 
	    $result=array();
	   foreach($categories as $category){
		      
		      if($category['name']=="Uncategorised" or $category['name']=="scheduling"){
				  				     
			  }
		      else{
				  array_push($result,$category);
			  }
	   }
	   return $result ;
	                        
  }

//custom end point to update order
add_action( 'rest_api_init', function () {
  register_rest_route( '/wc/v2/','update_order', array(
    'methods' => 'POST',
    'callback' => 'update_order_quantity',
  ) );
} );
function update_order_quantity($request){
	
	  $parameters = $request->get_params();
	  $order_id=$parameters['order_id'];
	  $product_id=$parameters['product_id'];
	  $quantity=$parameters['quantity'];
	
	  $order = wc_get_order($order_id);
      foreach ($order->get_items() as  $item_key => $item_values) {
		   $item_data = $item_values->get_data();
		   $line_item_id =$item_data['id'];
           $line_product_id = $item_data['product_id'];
		 
           if($line_product_id===$product_id){
			   $result= wc_update_order_item_meta($line_item_id, '_qty',$quantity);
			  
			   if($result){
				   
				    return "order update successfully ";
			   }
			   else{
				   return  "order update is failed";
			   }
			    
		   }
	}
  
      
}  
    

//redirect to a url aftre password reset 
add_action( 'after_password_reset','redirect_to_thankyou');

function redirect_to_thankyou(){
	 $url = site_url().'/thank-you/';
	 wp_redirect($url); 
    exit;
}
	?>